/// <reference types="Cypress" />

describe('Testes de Integração no Site Senium', { timeout: 30000 }, function() {
  beforeEach(() => {
    // Visite a página desejada antes de cada teste.
    cy.visit('https://mvp-senium-h.maracanau.ifce.edu.br/', { timeout: 20000 })
  })

  it('deve validar a seleção do botão "Quero me candidatar"', function() {
    cy.get('.button-forms', { timeout: 15000 }).should('be.visible').click();
   
  })

  it('deve validar a seleção do botão "FALE CONOSCO"', function() {
    cy.get('.button-navbar', { timeout: 15000 }).click();
   
  })

  it('deve validar a seleção do link do Facebook', function() {
    cy.get('[href="https://www.facebook.com/people/Senium-Longevidade-Saudável/61550724605373/"]', { timeout: 15000 }).click();

  })

  it('deve validar a seleção do link do Instagram', function() {
    cy.get('[href="https://www.instagram.com/seniumlongevidade/"]', { timeout: 15000 }).click();

  })
  it.only('deve validar a seleção do link do ', function() {
    cy.get('[href="https://www.linkedin.com/in/senium-longevidade-saudável-ba61b5284/"]').click();
})

})
